<?php

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode( '/', $uri );

	// we're returning JSON
	header('Content-Type: application/json; charset=utf-8');

	// Keep API key hidden
	$env = parse_ini_file(".env");
	$key = $env["API_KEY"];

	// TODO: uri validation here


	include "call_api.php";	
	$data = CallBusAPI($key);
	unset($key);

	// placeholder data
	/*
	$data = [
				"bus-times" => [
					["bus-number" => "16", "eta" => "289"],
					["bus-number" => "25", "eta" => "350"],
					["bus-number" => "39A", "eta" => "360"],
					["bus-number" => "25", "eta" => "450"],
					["bus-number" => "25", "eta" => "605"],
				],			
				"request-uri" => $uri,
			];
	*/
	
	echo $data;

	
?>