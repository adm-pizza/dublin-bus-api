<?php

	function CallBusAPI($key)
	{
		$url = "https://api.nationaltransport.ie/gtfsr/v2/TripUpdates?format=json";
	
	    $curl = curl_init();
	
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_HTTPHEADER, ["x-api-key: ".$key]);
	
	    $result = curl_exec($curl);
	
	    curl_close($curl);
	
	    return $result;
	}
	
?>