# Dublin Bus API

A PHP backend API for getting Dublin Bus GTFS data (real-time stop data).

WIP, current plan is for it to produce a custom data format that's fairly minimal.