#!/usr/bin/bash

curl https://www.transportforireland.ie/transitData/Data/GTFS_Dublin_Bus.zip -o GTFS.zip
mkdir GTFS
rm GTFS/*
unzip GTFS.zip -d ./GTFS
rm GTFS.zip

cd GTFS
# rename to csv, cause thats what it is!!

for i in *.txt; do
 mv -- "$i" "${i%.txt}.csv"
done